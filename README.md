# Badgr Extras

> Additional functionality for consumers of Badgr-based open badges.

## How to update to a new version?

1. Update "./extension/manifest.json" file at the "version" property.
2. Update "package.json" file at the "version" property.

## User Data Privacy Policy

### Personal or sensitive user data

1. Collect email address.
2. Collect BadgerID.
3. Access to JWT token and collect it through Local Storage (optional).
4. Authentication cookies (optional).

### This extension DOES handle personal or sensitive user data. To do:

1. Post a privacy policy in the Chrome Web Store Developer Dashboard, and
2. Handle the user data securely, including transmitting it via modern cryptography.

## Chrome Web Store important notes

- January 17, 2022: New Manifest V2 extensions will no longer be accepted by the Chrome Web Store. Developers may still push updates to existing Manifest V2 extensions, but no new Manifest V2 items may be submitted.
- January 2023: The Chrome browser will no longer run Manifest V2 extensions. Developers may no longer push updates to existing Manifest V2 extensions.
- Migration to V3 guide: https://developer.chrome.com/docs/extensions/mv3/mv3-migration-checklist/

> Version 12.0.0 migrated to New Manifest V3.
