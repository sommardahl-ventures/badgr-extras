var md = new Remarkable();

function getMarkdown(url) {
  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    headers: {
      "Content-Type": "text/plain; charset=utf-8",
    },
  };

  return $.ajax(settings);
}

function setCriteria() {
  var href = "";
  var button = 0;
  var url_parts;
  try {
    button = $("div.ng-star-inserted")
      .find("div.block-x-body")
      .find("a.button").length;
    href = document
      .querySelector("div.ng-star-inserted > a.textLink")
      .getAttribute("href");
    url_parts = href.split("/");
  } catch (e) {
    renderClaimBadgeButton("add_elements");
    return "";
  }

  var open_tag =
    '<bg-markdown id="criteria" class="markdown-dark4 markdown ng-star-inserted">';
  var close_tag = "</bg-markdown>";
  var len = url_parts.length;
  if (button > 0 && url_parts[len - 1].includes(".md")) {
    $.when(getMarkdown(href)).done(function (a1) {
      if (a1) {
        html_criteria = md.render(a1).replace("h1", "h2");
        $("div.ng-star-inserted")
          .find("div.u-margin-yaxis2x")
          .html(open_tag + html_criteria + close_tag);
        $("div.ng-star-inserted")
          .find("div.u-margin-yaxis2x")
          .removeClass("u-margin-yaxis2x");
      }
    });
  }
}
