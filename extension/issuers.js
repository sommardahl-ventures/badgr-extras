// TODO: check out this file. FYI, currently it doesn't work.

function createButtons() {
  $("a.pathwaycard").each(function (index) {
    var pathway = $(this).find("h2").text();
    var href = $(this).attr("href");
    // console.log(href.split("/")[3]);
    $(this).find("h2").attr("href", href);
    $(this)
      .find("h2")
      .replaceWith(
        '<a class="pathwaycard-x-title u-text-link" href=' +
          href +
          ">" +
          pathway +
          "</a>"
      );
    $(this).replaceWith(
      '<div id="pathway' +
        index +
        '" class="pathwaycard ng-star-inserted">' +
        $(this).html() +
        "</div>"
    );

    $("#pathway" + index)
      .find("div.pathwaycard-x-wrap")
      .append(
        '<button id="btn_' +
          index +
          '" style="margin-top: 10%;" class="button">Request Access</button>'
      );

    $("#btn_" + index).click(function () {
      $("#email_modal").find("h3").html(pathway).attr("id", href.split("/")[3]);
      $("body").find("#email_modal").css("display", "inline");
      $("body")
        .find("#sendEmail")
        .click(function (evt) {
          evt.stopImmediatePropagation();
          sendEmail();
        });
    });
  });
}

function closeEmailModal() {
  $("body").find("#email_modal").css("display", "none");
}

function sendEmail() {
  var user_email = $("body")
    .find("#email_modal")
    .find("#email")
    .val()
    .toLowerCase();
  var issuer_email = $("div.issuerhero")
    .find("a")
    .last()
    .attr("href")
    .split(":")[1];
  var pathway_name = $("#email_modal").find("h3").html();
  var pathway_id = $("#email_modal").find("h3").attr("id");

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user_email)) {
    var settings = {
      // url: `https://badge-claim.app/api/pathways/${pathway_id}/subscribe`,
      method: "POST",
      timeout: 0,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        from: user_email,
        to: issuer_email,
        pathway: pathway_name,
      }),
    };

    $.ajax(settings)
      .done(function () {
        $("#snackbar").addClass("show");
        setTimeout(function () {
          $("#snackbar").removeClass("show");
        }, 3000);
        closeEmailModal();
      })
      .fail(function () {
        $("#snackbar_error").html("Something bad happen with the request");
        $("#snackbar_error").addClass("show");
        setTimeout(function () {
          $("#snackbar_error").removeClass("show");
        }, 3000);
      });
  } else {
    $("#snackbar_error").html("Invalid Email");
    $("#snackbar_error").addClass("show");
    setTimeout(function () {
      $("#snackbar_error").removeClass("show");
    }, 3000);
  }
}

function addEmailModal() {
  let $modal = $(
    '<div id="snackbar">The request was sent successfully</div>' +
      '<dialog-layout role="dialog" id="email_modal" style="display: none;" dialogtitle="Claim Badge" class="dialog l-dialog dialog-is-active ng-star-inserted" aria-hidden="false">' +
      '<div id="snackbar_error"></div>' +
      '<div class="dialog-x-box o-container o-container-is-managed o-container-is-atleast480px o-container-is-atmost768px o-container-is-atmost1024px o-container-is-atmost1440px" style="">' +
      "<bg-dialog-messages></bg-dialog-messages>" +
      '<div class="l-flex l-flex-justifybetween u-padding-top2x u-padding-xaxis3x">' +
      '<h3 class="u-text u-text-h3 u-text-dark3 ng-star-inserted" id="null"></h3>' +
      '<div class="l-flex l-flex-0p5x">' +
      '<button id="close_modal" class="buttonicon ng-star-inserted" style="font-size: 16px;">X' +
      "</button>" +
      "</div>" +
      "</div>" +
      '<div class="u-padding-all3x">' +
      '<bg-recipient-verification-panel requestidentifiertitle="" requestidentifiermessage="Enter your email address to subscribe this pathway.">' +
      '<p class="ng-star-inserted"> Enter your email address to subscribe this pathway. </p>' +
      "<div>" +
      '<bg-formfield-text label="Email" class="forminput forminput-required ng-star-inserted">' +
      '<div class="forminput-x-labelrow">' +
      '<label class="forminput-x-label" for="email"> Email </label>' +
      "</div>" +
      '<div class="forminput-x-inputs">' +
      '<input type="email" name="email" id="email" placeholder="" class="ng-pristine ng-invalid ng-star-inserted ng-touched">' +
      "</div>" +
      "</bg-formfield-text>" +
      '<div class="l-stack l-stack-buttons l-stack-2x u-margin-top2x">' +
      '<button id="sendEmail" class="button">Continue</button>' +
      '<button id="close_modal_btn" class="button button-secondary ng-star-inserted">Cancel</button>' +
      "</div>" +
      "</div>" +
      "</bg-recipient-verification-panel>" +
      "</div>" +
      "</div>" +
      "</dialog-layout>"
  );
  $("body").find("div.issuerhero").before($modal);
  $("body").find("#close_modal").click(closeEmailModal);
  $("body").find("#close_modal_btn").click(closeEmailModal);
}

function renderPathwaysButton(message) {
  if (message === "add_elements") {
    var incoming_url = window.location.href;
    var base_url = "https://badgr.com/public/issuers/*";
    var pro_url = "https://acklenavenue.badgr.com/public/issuers/*";
    var codex_url = "https://codexacademy.badgr.com/public/issuers/*";
    var regex = new RegExp("/[^/]*$");
    var test = incoming_url.replace(regex, "/");
    test = test.substring(0, test.length - 1).replace(regex, "/");
    if (test === base_url || test === pro_url || test === codex_url) {
      setTimeout(function () {
        addEmailModal();
        createButtons();
      }, 2000);
    }
  }
}

// Deprecated: using content_scripts at manifest.json
// Verify response to content background service worker
// chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
//   console.log(request, sender, sendResponse);
//   renderPathwaysButton(request.message);
//   sendResponse({ farewell: "goodbye" });
// });

renderPathwaysButton("add_elements");
