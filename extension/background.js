// Opening an extension onboarding page in a new tab.
chrome.runtime.onInstalled.addListener((reason) => {
  if (reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.tabs.create({
      url: "onboarding.html",
    });
  }
});

// TODO: make requests here on the background service worker.

// TODO: make request trough browser API
