// @tutorial
// https://gokatz.me/blog/automate-your-chrome-extension-deployment-in-minutes/

// Can use Chrome Web Store API too
// https://developer.chrome.com/docs/webstore/using_webstore_api/

// Imports
const zipFolder = require("zip-folder");
const fileSystem = require("fs");
const chromeWebstoreUpload = require("chrome-webstore-upload");

// Getting all the credentials and IDs from `gitlab-ci.yml` file
const getCredentials = () => {
  return {
    CLIENT_ID: process.env.CLIENT_ID,
    CLIENT_SECRET: process.env.CLIENT_SECRET,
    REFRESH_TOKEN: process.env.REFRESH_TOKEN,
    EXTENSION_ID: process.env.EXTENSION_ID,
    SEM_MESSAGE: process.env.SEM_MESSAGE,
    VER_EXT: process.env.VER_EXT,
  };
};
const credentials = getCredentials();

// Initialize Chrome Web Store upload.
const webStore = chromeWebstoreUpload({
  extensionId: credentials.EXTENSION_ID,
  clientId: credentials.CLIENT_ID,
  clientSecret: credentials.CLIENT_SECRET,
  refreshToken: credentials.REFRESH_TOKEN,
});

let srcFolderName = "extension";
let zipName = "badgr_extras.zip";
let token = getToken() || ""; // Optional. One will be fetched if not provided.
var version = credentials.VER_EXT;
let parts = credentials.SEM_MESSAGE.split(" ");
if (parts.length === 3) {
  version = parts[2];
}

// Update version number at manifest.json file.
function updateVersion() {
  var json = JSON.parse(
    fileSystem.readFileSync("extension/manifest.json").toString()
  );
  json.version = version;
  fileSystem.writeFile(
    "extension/manifest.json",
    JSON.stringify(json, null, "\t"),
    (error, result) => {
      if (err) {
        console.error("Error while writing manifest.json file: ", error);
      } else {
        if (result) {
          console.log("Writing manifest.json file result: ", result);
        }
      }
    }
  );
}

// Pack extension in a zip file.
function compressExtension() {
  zipFolder(srcFolderName, zipName, (error) => {
    if (error) {
      console.error("Error generating .zip file: ", error);
    } else {
      console.log(
        `Successfully zipped the ${srcFolderName} directory and store as ${zipName}`
      );
      // Publish the extension
      console.log("Uploading .zip file with the latest version " + version);
      upload();
    }
  });
}

// Fetch Chrome Web Store token.
function getToken() {
  return webStore
    .fetchToken()
    .then((token) => {
      // Token is a string
      return token;
    })
    .catch((error) => {
      console.error("Error while trying to fetch token: ", error);
      process.exit(1);
    });
}

// Upload actual extension to Chrome Web Store.
function upload() {
  const extensionZipped = fileSystem.createReadStream(zipName);
  webStore
    .uploadExisting(extensionZipped, token)
    .then((res) => {
      // Response is a Resource Representation
      // https://developer.chrome.com/webstore/webstore_api/items#resource
      console.log(res);
      console.log("Successfully uploaded the ZIP");
      publish();
    })
    .catch((error) => {
      console.error("Error while uploading ZIP: ", error);
      process.exit(1);
    });
}

// Publish extension to Chrome Web Store.
function publish() {
  // Publish the uploaded zip
  const target = "default";
  webStore
    .publish(target, token)
    .then((res) => {
      // Response is documented here:
      // https://developer.chrome.com/webstore/webstore_api/items/publish
      console.log(res);
      console.log("Successfully published the newer version");
    })
    .catch((error) => {
      console.error("Error while publishing uploaded extension: ", error);
      process.exit(1);
    });
}

// Get a Chrome Web Store item.
function getItem() {
  const projection = "DRAFT"; // optional. Can also be 'PUBLISHED' but only "DRAFT" is supported at this time.
  webStore
    .get(projection, token)
    .then((res) => {
      // Response is documented here:
      // https://developer.chrome.com/docs/webstore/webstore_api/items/get
      console.log(res);
      console.log("Successfully got an item");
      return res;
    })
    .catch((error) => {
      console.error("Error while getting an item from webstore: ", error);
      process.exit(1);
    });
}

// Trigget functions.
updateVersion();
compressExtension();
